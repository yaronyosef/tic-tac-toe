import Vue from 'vue';
import Vuex from "vuex";

Vue.use(Vuex);

const state = () => {

  const players = ["X", "O"];

  let state = {
    players,
    currentPlayerSymbol: players[Math.floor(Math.random() * Math.floor(2))],
    squares: [
      { value: 2 }, { value: 7 }, { value: 6 },
      { value: 9 }, { value: 5 }, { value: 1 },
      { value: 4 }, { value: 3 }, { value: 8 }
    ],
    winningMatch: 15,
    gameStatus: 'initial',
    strikingSquares: []
  };

  return state;
};

const actions = {
  reset({ commit, state }) {
    state.gameStatus = 'initial';
    commit('reserSquares');
    commit('setStrikingSquares', []);
  },
  async setSquare({ commit, state, dispatch, getters }, index) {
    if (!await dispatch("isValidMove", index) || getters.gameWon) return false;
    const remainingNumbersToWin = state.winningMatch - state.squares[index].value;
    const pairSum = await dispatch("getPairSumOf", remainingNumbersToWin);
    commit('setSquareValue', index);
    if (pairSum) {
      state.gameStatus = "won";
      commit('setStrikingSquares', [...pairSum, state.squares[index].value]);
    } else {
      commit('switchPlayer');
    }
    return true;
  },
  getPairSumOf({ getters }, remainingNumbersToWin) {
    const currentPlayerSquares = getters.getCurrentPlayerSquaresIds.sort();
    if (currentPlayerSquares.length === 0) return;
    let leftPosition = 0;
    let rightPosition = currentPlayerSquares.length - 1;
    while (leftPosition < rightPosition) {
      const number1 = currentPlayerSquares[leftPosition];
      const number2 = currentPlayerSquares[rightPosition];
      const sumOfSquares = number1 + number2;
      if (sumOfSquares === remainingNumbersToWin)
        return [number1, number2];
      else if (sumOfSquares < remainingNumbersToWin) {
        leftPosition++;
      } else {
        rightPosition--;
      }
    }
    return false;
  },
  isValidMove: ({ state }, index) => {
    return state.squares[index].symbol === undefined;
  }
};

const mutations = {
  setSquareValue(state, index) {
    state.squares[index].symbol = state.currentPlayerSymbol;
  },
  switchPlayer(state) {
    state.currentPlayerSymbol = state.currentPlayerSymbol === "X" ? "O" : "X";
  },
  reserSquares(state) {
    state.squares = state.squares.map(square => ({ value: square.value }));
  },
  setStrikingSquares(state, squares) {
    state.strikingSquares = squares;
  },
};

const getters = {
  getCurrentPlayerSquaresIds: (state) => {
    return state.squares.
    filter(square => square.symbol === state.currentPlayerSymbol).
    map(square => square.value);
  },
  currentPlayerSymbol: state => state.players[state.currentPlayerSymbol],
  gameWon: (state) => {
    return state.gameStatus === 'won';
  }
};

const store = new Vuex.Store({
  state,
  getters,
  actions,
  mutations
});

export default store;
export const strict = true;
